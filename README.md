# IDS-721-Cloud-Computing :computer:

## Mini Project 9 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Create a website using Streamlit
* Connect to an open source LLM (Hugging Face)
* Deploy model via Streamlit or other service (accessible via browser)

## :ballot_box_with_check: Grading Criteria
* __Functioning Web App__ 25%
* __Connection to Open Source LLM__ 25%
* __Documentation__ 20%
* __Aesthetics/Creativity of Site__ 15%
* __Chatbot Performance__ 15%
* __Bonus (hosting EC2 or provider outside of Streamlit)__ +15%

## :ballot_box_with_check: Deliverables
* Rust code
* Screenshots
* Explanation writeup

## :ballot_box_with_check: Main Progress
1. __Web App Python code__
```python
import streamlit as st
from transformers import pipeline

# Load a pre-trained model pipeline for sentiment analysis or text classification
model = pipeline(
    "text-classification", model="textdetox/xlmr-large-toxicity-classifier"
)

st.title("Toxicity Detection")
user_input = st.text_area("Enter text to analyze")

if st.button("Analyze"):
    # Model prediction
    result = model(user_input)
    # Simplify to just display 'Toxic' or 'Not Toxic'
    if result[0]["label"] == "LABEL_1":  # Adjust based on your model's output
        st.write("Toxic")
    else:
        st.write("Not Toxic")

```
- This code determines whether the input text is toxic.

2. __Connect to an Open Source LLM__
```python
import streamlit as st
from transformers import pipeline

# Load a pre-trained model pipeline for sentiment analysis or text classification
model = pipeline(
    "text-classification", model="textdetox/xlmr-large-toxicity-classifier"
)
```
- The project utilizes the 'toxicity classifier' from the transformers' pipeline, leveraging the open-source LLM model to assess text toxicity.

3. __Aesthetics/Creativity of Site__
- Website Page
![Page](https://github.com/suim-park/IDS721-Mini-Project-9/assets/143478016/9591b812-a45c-489c-ad70-66255b720bcb)

- Option: You can choose several options on this website.
![Option](https://github.com/suim-park/IDS721-Mini-Project-9/assets/143478016/5b57a70f-4fa3-41da-8fbc-7bfccccce347)
![Option-1](https://github.com/suim-park/IDS721-Mini-Project-9/assets/143478016/58f8cb6d-4416-4ca0-a5a7-e0f330f927a1)

4. __Chatbot Performance__
- Toxic
![Toxic](https://github.com/suim-park/IDS721-Mini-Project-9/assets/143478016/a54492fa-d760-42cb-81e9-e3a57171a194)

- Not Toxic
![Non Toxic](https://github.com/suim-park/IDS721-Mini-Project-9/assets/143478016/9c98e09d-0129-4017-9d7e-3b8a0d9232ca)

5. __Deploy Streamlit App on Local and via AWS EC2__

(1) Local Deployment: Local deployment can be achieved using the command line below.
```bash
streamlit run streamlit-app.py
```
![Local Deployment](https://github.com/suim-park/IDS721-Mini-Project-9/assets/143478016/ea2a5739-349b-4b14-b24d-ae8aa68fed4b)
- If you click the `Local URL`, the Streamlit App will be shown on the local server.

(2) __EC2 Deployment__: Deployment on an EC2 virtual machine can be accomplished with the following command lines, detailing the process for deploying a Streamlit App on AWS EC2 on port 8501.

```bash
sudo apt update
```
```bash
sudo apt-get update
```
```bash
sudo apt upgrade -y
```
```bash
sudo apt install git curl unzip tar make sudo vim wget -y
```
```bash
git clone "https://gitlab.com/dukeaiml/IDS721/ids721-mini-project-9-sp699/"
```
```bash
sudo apt install python3-pip
```
```bash
pip3 install -r requirements.txt
```
```bash
#Temporary running
python3 -m streamlit run streamlit-app.py
```
```bash
#Permanent running
nohup python3 -m streamlit run streamlit-app.py
```

- Deploy the app on AWS EC2
![EC2 Deployment](https://github.com/suim-park/Mini-Project-1/assets/143478016/e8cef6d5-7e39-4dfd-8ef3-562a734da73a)

- The screenshot depicts the deployment of an app on AWS EC2, accessible on port 8501. The public deployed url address is 'http://http://18.119.235.150:8501/'.
![EC2 Deployment-1](https://github.com/suim-park/Mini-Project-1/assets/143478016/4976116b-1e6b-4789-abff-fcd68538c2bf)